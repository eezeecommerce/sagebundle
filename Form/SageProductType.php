<?php

namespace eezeecommerce\SageBundle\Form;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SageProductType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sage_reference')
            ->add('product', 'entity', array(
                'class' => 'eezeecommerceProductBundle:Product',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder("p")
                        ->select("p")
                        ->orderBy("p.stock_code", "ASC");
                },
                'property' => 'stock_code',
                'multiple' => false,
                "required" => true,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\SageBundle\Entity\SageProduct'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_sagebundle_sageproduct';
    }
}
