<?php

namespace eezeecommerce\SageBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class SageProductOptionsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sage_reference')
            ->add('option', 'entity', array(
                'class' => 'eezeecommerceProductBundle:Options',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder("o")
                        ->select("o")
                        ->orderBy("o.stock_code", "ASC");
                },
                'property' => 'stock_code',
                'multiple' => false,
                "required" => true,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\SageBundle\Entity\SageProductOptions'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_sagebundle_sageproductoptions';
    }
}
