<?php

namespace eezeecommerce\SageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SageProductOptions
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class SageProductOptions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="\eezeecommerce\ProductBundle\Entity\Options")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     */
    private $option;

    /**
     * @var string
     *
     * @ORM\Column(name="sage_reference", type="string")
     */
    private $sage_reference;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sageReference
     *
     * @param string $sageReference
     *
     * @return SageProductOptions
     */
    public function setSageReference($sageReference)
    {
        $this->sage_reference = $sageReference;

        return $this;
    }

    /**
     * Get sageReference
     *
     * @return string
     */
    public function getSageReference()
    {
        return $this->sage_reference;
    }

    /**
     * Set option
     *
     * @param \eezeecommerce\ProductBundle\Entity\Options $option
     *
     * @return SageProductOptions
     */
    public function setOption(\eezeecommerce\ProductBundle\Entity\Options $option = null)
    {
        $this->option = $option;

        return $this;
    }

    /**
     * Get option
     *
     * @return \eezeecommerce\ProductBundle\Entity\Options
     */
    public function getOption()
    {
        return $this->option;
    }
}
