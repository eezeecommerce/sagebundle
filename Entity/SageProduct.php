<?php

namespace eezeecommerce\SageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SageProduct
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class SageProduct
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="\eezeecommerce\ProductBundle\Entity\Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="sage_reference", type="string")
     */
    private $sage_reference;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sageReference
     *
     * @param string $sageReference
     *
     * @return SageProduct
     */
    public function setSageReference($sageReference)
    {
        $this->sage_reference = $sageReference;

        return $this;
    }

    /**
     * Get sageReference
     *
     * @return string
     */
    public function getSageReference()
    {
        return $this->sage_reference;
    }

    /**
     * Set product
     *
     * @param \eezeecommerce\ProductBundle\Entity\Product $product
     *
     * @return SageProduct
     */
    public function setProduct(\eezeecommerce\ProductBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \eezeecommerce\ProductBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
