<?php

namespace eezeecommerce\SageBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SageProductVariations
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class SageProductVariations
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="\eezeecommerce\ProductBundle\Entity\Variants")
     * @ORM\JoinColumn(name="variant_id", referencedColumnName="id")
     */
    private $variant;

    /**
     * @var string
     *
     * @ORM\Column(name="sage_reference", type="string")
     */
    private $sage_reference;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sageReference
     *
     * @param string $sageReference
     *
     * @return SageProductVariations
     */
    public function setSageReference($sageReference)
    {
        $this->sage_reference = $sageReference;

        return $this;
    }

    /**
     * Get sageReference
     *
     * @return string
     */
    public function getSageReference()
    {
        return $this->sage_reference;
    }

    /**
     * Set variant
     *
     * @param \eezeecommerce\ProductBundle\Entity\Variants $variant
     *
     * @return SageProductVariations
     */
    public function setVariant(\eezeecommerce\ProductBundle\Entity\Variants $variant = null)
    {
        $this->variant = $variant;

        return $this;
    }

    /**
     * Get variant
     *
     * @return \eezeecommerce\ProductBundle\Entity\Variants
     */
    public function getVariant()
    {
        return $this->variant;
    }
}
