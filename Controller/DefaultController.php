<?php

namespace eezeecommerce\SageBundle\Controller;

use eezeecommerce\SageBundle\Entity\SageProduct;
use eezeecommerce\SageBundle\Entity\SageProductOptions;
use eezeecommerce\SageBundle\Entity\SageProductVariations;
use eezeecommerce\SageBundle\Form\SageProductOptionsType;
use eezeecommerce\SageBundle\Form\SageProductType;
use eezeecommerce\SageBundle\Form\SageProductVariationsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/admin/settings/sage", name="eezeecommerce_settings_sage_index")
     * @Template("eezeecommerceSageBundle:Admin:index.html.twig")
     */
    public function indexAction()
    {
        return array();
    }

    /**
     * @Route("/admin/settings/sage/products", name="eezeecommerce_settings_sage_product_index")
     * @Template("eezeecommerceSageBundle:Admin:product_index.html.twig")
     */
    public function productsAction()
    {
        return array();
    }

    /**
     * @Route("/admin/settings/sage/options", name="eezeecommerce_settings_sage_options_index")
     * @Template("eezeecommerceSageBundle:Admin:options_index.html.twig")
     */
    public function optionsAction()
    {
        return array();
    }

    /**
     * @Route("/admin/settings/sage/variants", name="eezeecommerce_settings_sage_variants_index")
     * @Template("eezeecommerceSageBundle:Admin:variants_index.html.twig")
     */
    public function variantsAction()
    {
        return array();
    }

    /**
     * @Route("/admin/settings/sage/products/add", name="eezeecommerce_settings_sage_product_add")
     * @Template("eezeecommerceSageBundle:Admin:product_add.html.twig")
     */
    public function productAddAction(Request $request)
    {
        $product = new SageProduct();

        $form = $this->createForm(new SageProductType(), $product);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute("eezeecommerce_settings_sage_product_index");
        }

        return array("form" => $form->createView());
    }

    /**
     * @Route("/admin/settings/sage/options/add", name="eezeecommerce_settings_sage_option_add")
     * @Template("eezeecommerceSageBundle:Admin:option_add.html.twig")
     */
    public function optionsAddAction(Request $request)
    {
        $product = new SageProductOptions();

        $form = $this->createForm(new SageProductOptionsType(), $product);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute("eezeecommerce_settings_sage_options_index");
        }

        return array("form" => $form->createView());
    }

    /**
     * @Route("/admin/settings/sage/variant/add", name="eezeecommerce_settings_sage_variant_add")
     * @Template("eezeecommerceSageBundle:Admin:variants_add.html.twig")
     */
    public function variantsAddAction(Request $request)
    {
        $product = new SageProductVariations();

        $form = $this->createForm(new SageProductVariationsType(), $product);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute("eezeecommerce_settings_sage_variants_index");
        }

        return array("form" => $form->createView());
    }

    /**
     * @Route("/admin/settings/sage/products/data/ajax", name="_eezeecommerce_admin_setting_sage_product_index_ajax")
     */
    public function productsAjaxAction()
    {
        $sageProducts = $this->getDoctrine()->getRepository("eezeecommerceSageBundle:SageProduct")
            ->findAll();

        $array = array();
        foreach ($sageProducts as $product) {
            $tmp = array();
            $tmp[] = $product->getId();
            $tmp[] = "<a href='".$this->generateUrl('eezeecommerce_settings_sage_product_edit', ['id' => $product->getId()])."'>".$product->getProduct()->getStockCode()."</a>";
            $tmp[] = $product->getSageReference();
            $array["data"][] = $tmp;
        }

        return new JsonResponse($array);
    }

    /**
     * @Route("/admin/settings/sage/options/data/ajax", name="_eezeecommerce_admin_setting_sage_option_index_ajax")
     */
    public function optionsAjaxAction()
    {
        $sageProductOptions = $this->getDoctrine()->getRepository("eezeecommerceSageBundle:SageProductOptions")
            ->findAll();

        $array = array();
        foreach ($sageProductOptions as $options) {
            $tmp = array();
            $tmp[] = $options->getId();
            $tmp[] = "<a href='".$this->generateUrl('eezeecommerce_settings_sage_option_edit', ['id' => $options->getId()])."'>".$options->getOption()->getStockCode()."</a>";
            $tmp[] = $options->getSageReference();

            $array["data"][] = $tmp;
        }

        return new JsonResponse($array);
    }

    /**
     * @Route("/admin/settings/sage/variants/data/ajax", name="_eezeecommerce_admin_setting_sage_variants_index_ajax")
     */
    public function variantsAjaxAction()
    {
        $sageProductVariations = $this->getDoctrine()->getRepository("eezeecommerceSageBundle:SageProductVariations")
            ->findAll();

        $array = array();
        foreach ($sageProductVariations as $variant) {
            $tmp = array();
            $tmp[] = $variant->getId();
            $tmp[] = "<a href='".$this->generateUrl('eezeecommerce_settings_sage_variant_edit', ['id' => $variant->getId()])."'>".$variant->getVariant()->getStockCode()."</a>";
            $tmp[] = $variant->getSageReference();

            $array["data"][] = $tmp;
        }

        return new JsonResponse($array);
    }

    /**
     * @Route("/admin/settings/sage/products/modify/{id}", name="eezeecommerce_settings_sage_product_edit")
     * @Template("eezeecommerceSageBundle:Admin:product_add.html.twig")
     */
    public function productEditAction(Request $request, $id)
    {
        $product = $this->getDoctrine()->getRepository("eezeecommerceSageBundle:SageProductOptions")
            ->find($id);

        $form = $this->createForm(new SageProductType(), $product);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute("eezeecommerce_settings_sage_product_index");
        }

        return array("form" => $form->createView());
    }


    /**
     * @Route("/admin/settings/sage/variant/modify/{id}", name="eezeecommerce_settings_sage_variant_edit")
     * @Template("eezeecommerceSageBundle:Admin:variants_add.html.twig")
     */
    public function variantsEditAction(Request $request, $id)
    {
        $product = $this->getDoctrine()->getRepository("eezeecommerceSageBundle:SageProductVariations")
            ->find($id);

        $form = $this->createForm(new SageProductVariationsType(), $product);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute("eezeecommerce_settings_sage_variants_index");
        }

        return array("form" => $form->createView());
    }

    /**
     * @Route("/admin/settings/sage/options/modify/{id}", name="eezeecommerce_settings_sage_option_edit")
     * @Template("eezeecommerceSageBundle:Admin:option_add.html.twig")
     */
    public function optionsEditAction(Request $request, $id)
    {
        $product = $this->getDoctrine()->getRepository("eezeecommerceSageBundle:SageProductOptions")
            ->find($id);

        $form = $this->createForm(new SageProductOptionsType(), $product);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute("eezeecommerce_settings_sage_options_index");
        }

        return array("form" => $form->createView());
    }
}
