<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 11/07/16
 * Time: 18:53
 */

namespace eezeecommerce\SageBundle\EventListener;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use eezeecommerce\AdminBundle\AdminEvents;
use eezeecommerce\AdminBundle\Event\AdminMenuEvent;

class AdminMenuEventSubscriber implements EventSubscriberInterface
{

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return array(
            AdminEvents::ADMIN_MENU_COMPLETED => array(
                array("onMenuComplete", 10)
            )
        );
    }

    public function onMenuComplete(AdminMenuEvent $event)
    {
        $menu = $event->getMenu();

        $menu["settings"]["children"][] =
            array(
                "name" => "Sage Integration",
                "link" => "eezeecommerce_settings_sage_index",
            );

        $event->setMenu($menu);
    }
}